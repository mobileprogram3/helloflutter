import 'package:flutter/material.dart';


void main () => runApp(HelloFlutterApp());
class  HelloFlutterApp extends StatefulWidget {
  @override
  _MystatefulWidgetState createState() => _MystatefulWidgetState();
}
String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String japanishGreeting = "Kon'nichiwa Flutter";
String thaiGreeting = "Sawaddee Flutter";
class _MystatefulWidgetState extends State<HelloFlutterApp> {
  String displayText = englishGreeting;

  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar:AppBar(
            title: Text(
              "Hello Flutter"
            ),
            leading: Icon(Icons.home),
            actions: <Widget>[
              IconButton(onPressed: () {

                setState(() {
                  displayText = displayText == englishGreeting?
                  spanishGreeting :englishGreeting;
                });



              },
                  icon: Icon(Icons.refresh)),
              IconButton(onPressed: () {

                setState(() {
                  displayText = displayText == englishGreeting?
                  japanishGreeting :englishGreeting;
                });

              }, icon: Icon(Icons.change_circle)),
              IconButton(onPressed: () {

                setState(() {
                  displayText = displayText == englishGreeting?
                  thaiGreeting :englishGreeting;
                });

              }, icon: Icon(Icons.language)),


            ],

          ) ,
          body: Center(
            child: Text(
              displayText,
              style: TextStyle(fontSize: 24
              ),
            ),
          ),

        )


    );
  }

}












// class HelloFlutterApp extends StatelessWidget{
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//         home: Scaffold(
//           appBar:AppBar(
//             title: Text(
//               "Hello Flutter"
//             ),
//             leading: Icon(Icons.home),
//             actions: <Widget>[
//               IconButton(onPressed: () {},
//                   icon: Icon(Icons.refresh))
//             ],
//           ) ,
//           body: Center(
//             child: Text(
//               "Hello Flutter",
//               style: TextStyle(fontSize: 24
//               ),
//             ),
//           ),
//
//         )
//
//
//     );
//   }
//
// }
